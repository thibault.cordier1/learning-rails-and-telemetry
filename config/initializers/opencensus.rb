OpenCensus.configure do |c|

  c.trace.default_max_attributes = 64
  c.trace.exporter = OpenCensus::Trace::Exporters::JaegerExporter.new(
      service_name: 'myTelemetry',
      host: 'localhost', # default to 'localhost'
      port: '6831', # default to 6831
      tags: { 'something': 'useful' },
      max_packet_length: 12_345, # config if you want something smaller than DEFAULT_MAX_LENGTH,
      protocol_class: ::Thrift::CompactProtocol # currently supporting only compact protocol
  )
end
